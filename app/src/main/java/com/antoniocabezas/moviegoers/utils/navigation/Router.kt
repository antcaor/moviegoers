package com.antoniocabezas.moviegoers.utils.navigation

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import com.antoniocabezas.moviegoers.presentation.ui.activities.DetailsActivity

object Router {
    val TAG: String = Router::class.java.simpleName
    const val MovieId: String = "MovieId"

    fun navigateToMovieDetails(currentActivity: AppCompatActivity, movieId: String){
        val mIntent = Intent(currentActivity, DetailsActivity::class.java)
        mIntent.putExtra(MovieId, movieId)
        currentActivity.startActivity(mIntent)
    }
}