package com.antoniocabezas.moviegoers.utils.recyclerview

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View

class RecyclerViewItemClickListener(
    context: Context, recyclerview: RecyclerView,
    private val listener: RecyclerTouchListener
) : RecyclerView.OnItemTouchListener {

    private val mGestureDetector: GestureDetector

    interface RecyclerTouchListener {

        fun onClickItem(v: View, position: Int)
        fun onLongClickItem(v: View, position: Int)
    }

    init {
        mGestureDetector = GestureDetector(context,
            object : GestureDetector.SimpleOnGestureListener() {
                override fun onLongPress(e: MotionEvent) {
                    // We find the view
                    val v = recyclerview.findChildViewUnder(e.x, e.y)
                    // Notify the event
                    listener.onLongClickItem(v, recyclerview.getChildAdapterPosition(v))
                }

                override fun onSingleTapUp(e: MotionEvent): Boolean {
                    val v = recyclerview.findChildViewUnder(e.x, e.y)
                    // Notify the event
                    listener.onClickItem(v, recyclerview.getChildAdapterPosition(v))
                    return true
                }
            })
    }

    override fun onInterceptTouchEvent(recyclerview: RecyclerView, e: MotionEvent): Boolean {
        val mChild = recyclerview.findChildViewUnder(e.x, e.y)
        return mChild != null && mGestureDetector.onTouchEvent(e)
    }

    override fun onTouchEvent(recyclerview: RecyclerView, e: MotionEvent) {

    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

    }
}
