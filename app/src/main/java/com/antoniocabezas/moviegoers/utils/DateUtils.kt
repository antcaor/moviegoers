package com.antoniocabezas.moviegoers.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils{

    fun getCalculatedDate(dateFormat: String, days: Int): String {
        val cal = Calendar.getInstance()
        val s = SimpleDateFormat(dateFormat)
        cal.add(Calendar.DAY_OF_YEAR, days)
        return s.format(cal.time)
    }
}