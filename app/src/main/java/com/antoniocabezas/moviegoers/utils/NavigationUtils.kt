package com.antoniocabezas.moviegoers.utils

import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import com.antoniocabezas.moviegoers.presentation.ui.activities.BaseActivity

object NavigationUtils {

    fun <T> navigateTo(activityFrom: BaseActivity, activityTo: Class<T>) {
        startActivity(activityFrom, Intent(activityFrom, activityTo), null)
        activityFrom.finish()
    }
}