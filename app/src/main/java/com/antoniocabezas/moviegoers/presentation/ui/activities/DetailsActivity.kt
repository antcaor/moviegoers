package com.antoniocabezas.moviegoers.presentation.ui.activities

import android.os.Bundle
import com.antoniocabezas.moviegoers.R
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent
import com.antoniocabezas.moviegoers.di.modules.ViewModelModule

class DetailsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
    }
}
