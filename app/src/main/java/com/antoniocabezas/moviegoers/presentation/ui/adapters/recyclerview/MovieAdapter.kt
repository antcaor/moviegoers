package com.antoniocabezas.moviegoers.presentation.ui.adapters.recyclerview

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.antoniocabezas.moviegoers.R
import com.antoniocabezas.moviegoers.domain.models.Movie
import com.antoniocabezas.moviegoers.presentation.ui.adapters.recyclerview.viewholders.MovieViewHolder
import com.bumptech.glide.Glide

class MovieAdapter(val movies: MutableLiveData<ArrayList<Movie>>?, val context: Context?) :
    RecyclerView.Adapter<MovieViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.recyclerview_film_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return movies?.value?.size ?: 0
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        context?.let { mContext ->
            movies?.value?.get(position).let { mMovie ->
                holder.movieTitle.text = mMovie?.title

                mMovie?.backdropPath?.let { mBackdropPath ->
                    Glide.with(context)
                        .load(mBackdropPath)
                        .into(holder.movieImage)
                }

                mMovie?.voteAverage?.let { mVoteAverage ->
                    holder.movieVoteAverage.text = mVoteAverage.toString()
                }

                mMovie?.status?.let { mStatus ->
                    holder.movieSubtitle.text = mStatus
                }
            }
        }
    }

}