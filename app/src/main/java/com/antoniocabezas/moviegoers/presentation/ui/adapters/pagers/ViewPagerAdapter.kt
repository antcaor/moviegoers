package com.antoniocabezas.moviegoers.presentation.ui.adapters.pagers

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.antoniocabezas.moviegoers.presentation.ui.fragments.BaseFragment

class ViewPagerAdapter(val fragmentManager: FragmentManager, val fragments: ArrayList<BaseFragment>) :
    FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

}