package com.antoniocabezas.moviegoers.presentation.viewmodels

import com.antoniocabezas.moviegoers.MoviegoersApp
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent
import com.antoniocabezas.moviegoers.di.modules.ViewModelModule
import com.antoniocabezas.moviegoers.presentation.viewmodels.base.BaseViewModel

class DetailsViewModel : BaseViewModel() {

    companion object {
        val TAG: String = DetailsViewModel::class.java.simpleName
    }

    override fun injectDependencies(applicationComponent: ApplicationComponent) {
        applicationComponent.plus(ViewModelModule(this)).inject(this)
    }

    init {
        injectDependencies(MoviegoersApp.appComponent)
    }
}