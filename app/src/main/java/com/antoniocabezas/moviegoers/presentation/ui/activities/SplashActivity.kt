package com.antoniocabezas.moviegoers.presentation.ui.activities

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import com.antoniocabezas.moviegoers.R
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent
import com.antoniocabezas.moviegoers.di.modules.ViewModelModule
import com.antoniocabezas.moviegoers.utils.NavigationUtils.navigateTo

class SplashActivity : BaseActivity() {

    companion object {
        val TAG: String = SplashActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onResume() {
        super.onResume()
        Handler().postDelayed({
            navigateTo(this, MainActivity::class.java)
        }, 1000)
    }
}
