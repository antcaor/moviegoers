package com.antoniocabezas.moviegoers.presentation.ui.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.antoniocabezas.moviegoers.R
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent

class PopularFilmsFragment : BaseFragment() {

    companion object {
        val TAG: String = PopularFilmsFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_popular_films, container, false)
    }
}
