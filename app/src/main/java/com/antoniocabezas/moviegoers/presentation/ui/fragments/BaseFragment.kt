package com.antoniocabezas.moviegoers.presentation.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment

abstract class BaseFragment : Fragment() {
    companion object {
        val TAG: String = BaseFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}