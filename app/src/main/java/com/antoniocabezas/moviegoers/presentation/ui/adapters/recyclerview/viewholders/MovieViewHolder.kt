package com.antoniocabezas.moviegoers.presentation.ui.adapters.recyclerview.viewholders

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.antoniocabezas.moviegoers.R

class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val movieLayout: CardView = itemView.findViewById(R.id.movie_layout)
    val movieImage: ImageView = itemView.findViewById(R.id.movie_image)
    val movieVoteAverage: TextView = itemView.findViewById(R.id.movie_vote_average)
    val movieTitle: TextView = itemView.findViewById(R.id.movie_title)
    val movieSubtitle: TextView = itemView.findViewById(R.id.movie_subtitle)
}