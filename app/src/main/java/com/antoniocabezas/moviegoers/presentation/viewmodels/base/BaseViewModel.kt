package com.antoniocabezas.moviegoers.presentation.viewmodels.base

import android.arch.lifecycle.ViewModel
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent

abstract class BaseViewModel : ViewModel() {

    companion object {
        val TAG: String = BaseViewModel::class.java.simpleName
    }

    abstract fun injectDependencies(applicationComponent: ApplicationComponent)
}