package com.antoniocabezas.moviegoers.presentation.ui.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.antoniocabezas.moviegoers.R
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent

class RandomFilmsFragment : BaseFragment() {

    companion object {
        val TAG: String = RandomFilmsFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_random_films, container, false)
    }
}
