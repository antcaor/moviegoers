package com.antoniocabezas.moviegoers.presentation.ui.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.antoniocabezas.moviegoers.R
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : BaseFragment(), View.OnFocusChangeListener {

    companion object {
        val TAG: String = SearchFragment::class.java.simpleName
        const val ANIMATION_DURATION: Long = 300
        const val INITIAL_SEARCH_EDIT_TEXT_ALPHA = 0.5f
        const val FINAL_SEARCH_EDIT_TEXT_ALPHA = 1f

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.setListeners()
    }

    override fun onFocusChange(view: View?, hasFocus: Boolean) {
        when (view?.id) {
            R.id.search_edit_text -> handleSearchEditTextFocus(hasFocus)
        }
    }

    private fun setListeners() {
        search_edit_text.onFocusChangeListener = this
    }

    private fun handleSearchEditTextFocus(hasFocus: Boolean) {
        if (hasFocus) {
            search_edit_text.animate().alpha(FINAL_SEARCH_EDIT_TEXT_ALPHA).setDuration(ANIMATION_DURATION).start()
        } else {
            search_edit_text.animate().alpha(INITIAL_SEARCH_EDIT_TEXT_ALPHA).setDuration(ANIMATION_DURATION).start()
        }
    }
}
