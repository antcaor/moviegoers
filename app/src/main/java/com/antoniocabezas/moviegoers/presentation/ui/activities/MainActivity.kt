package com.antoniocabezas.moviegoers.presentation.ui.activities

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.antoniocabezas.moviegoers.R
import com.antoniocabezas.moviegoers.domain.interactors.implementations.GetUpdatedMoviesInteractorImpl
import com.antoniocabezas.moviegoers.presentation.ui.adapters.pagers.ViewPagerAdapter
import com.antoniocabezas.moviegoers.presentation.ui.fragments.*
import com.antoniocabezas.moviegoers.presentation.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class MainActivity : BaseActivity(), ViewPager.OnPageChangeListener {

    companion object {
        val TAG: String = MainActivity::class.java.simpleName
    }

    private lateinit var mPreviousMenuItem: MenuItem
    private var mViewModel: MainViewModel? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_updated -> {
                viewpager.currentItem = 0
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_popular -> {
                viewpager.currentItem = 1
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_random -> {
                viewpager.currentItem = 2
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorites -> {
                viewpager.currentItem = 3
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "Executing life cycle event on create.")
        super.onCreate(savedInstanceState)

        this.setContentView(R.layout.activity_main)
        this.setupViewModel()
        this.setupViewPager()
        this.setupListeners()

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        if (::mPreviousMenuItem.isInitialized) {
            this.mPreviousMenuItem.isChecked = false
        } else {
            navigation.menu.getItem(0).isChecked = false
        }

        navigation.menu.getItem(position).isChecked = false
        this.mPreviousMenuItem = navigation.menu.getItem(position)
    }

    override fun dispatchTouchEvent(event: MotionEvent?): Boolean {
        if (event?.action == MotionEvent.ACTION_DOWN) {
            val mView = currentFocus
            if (mView is EditText) {
                val mOutRect = Rect()
                mView.getGlobalVisibleRect(mOutRect)
                if (!mOutRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    mView.clearFocus()
                    val mInputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    mInputMethodManager.hideSoftInputFromWindow(mView.windowToken, 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    private fun setupViewPager() {
        Log.d(TAG, "Setting up view pager.")
        val mFragments: ArrayList<BaseFragment> = arrayListOf()
        mFragments.add(UpdatedFragment())
        mFragments.add(PopularFilmsFragment())
        mFragments.add(RandomFilmsFragment())
        mFragments.add(FavoritesFragment())
        viewpager.adapter = ViewPagerAdapter(supportFragmentManager, mFragments)
    }

    private fun setupListeners() {
        Log.d(TAG, "Setting up listeners.")
        viewpager.addOnPageChangeListener(this)
    }

    private fun setupViewModel() {
        Log.d(TAG, "Setting up viewmodel.")
        this.mViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        this.mViewModel?.setGetUpdatedMoviesInteractor(GetUpdatedMoviesInteractorImpl(this.mViewModel!!, Dispatchers.IO))
    }
}
