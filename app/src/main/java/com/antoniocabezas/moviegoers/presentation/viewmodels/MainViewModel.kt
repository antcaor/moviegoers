package com.antoniocabezas.moviegoers.presentation.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent
import com.antoniocabezas.moviegoers.di.modules.ViewModelModule
import com.antoniocabezas.moviegoers.domain.interactors.GetUpdatedMoviesInteractor
import com.antoniocabezas.moviegoers.domain.interactors.base.Interactor
import com.antoniocabezas.moviegoers.domain.models.Movie
import com.antoniocabezas.moviegoers.presentation.viewmodels.base.BaseViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class MainViewModel : BaseViewModel(), GetUpdatedMoviesInteractor.GetUpdatedMoviesCallback {


    companion object {
        val TAG: String = MainViewModel::class.java.simpleName
    }

    private lateinit var mGetUpdatedMoviesInteractor: GetUpdatedMoviesInteractor
    var mGetUpdatedMoviesInteractorState: MutableLiveData<Interactor.State> = MutableLiveData()

    var mUpdatedMovies: MutableLiveData<ArrayList<Movie>> = MutableLiveData()

    init {
        mUpdatedMovies.postValue(arrayListOf())
    }

    override fun injectDependencies(applicationComponent: ApplicationComponent) {
        applicationComponent.plus(ViewModelModule(this)).inject(this)
    }

    fun setGetUpdatedMoviesInteractor(interactor: GetUpdatedMoviesInteractor) {
        this.mGetUpdatedMoviesInteractor = interactor
    }

    suspend fun getUpdatedMovies(daysAgo: Int, page: Int) {
        if (::mGetUpdatedMoviesInteractor.isInitialized) {
            GlobalScope.async {
                Log.d(TAG, "Getting updated movies.")
                mGetUpdatedMoviesInteractor.setParams(daysAgo, page)
                mGetUpdatedMoviesInteractor.execute()
            }.await()
        }
    }

    override fun onGettingUpdatedMovies() {
        Log.d(TAG, "Getting updated movies.")
        mGetUpdatedMoviesInteractorState.postValue(Interactor.State.LOADING)
    }

    override fun onGettingUpdatedMoviesFailure(error: Interactor.Error) {
        Log.d(TAG, "Getting updated movies failure: $error.")
        mGetUpdatedMoviesInteractorState.postValue(Interactor.State.UNEXPECTED)
    }

    override fun onUpdatedMovieReceived(movie: Movie) {
        Log.d(TAG, "Getting updated movies success.")
        val mAuxArray = mUpdatedMovies.value
        mAuxArray?.add(movie)
        this.mUpdatedMovies.postValue(mAuxArray)
        mGetUpdatedMoviesInteractorState.postValue(Interactor.State.SUCCESSFUL)
    }
}