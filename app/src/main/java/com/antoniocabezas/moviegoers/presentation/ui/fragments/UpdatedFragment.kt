package com.antoniocabezas.moviegoers.presentation.ui.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.antoniocabezas.moviegoers.R
import com.antoniocabezas.moviegoers.domain.interactors.base.Interactor
import com.antoniocabezas.moviegoers.domain.models.Movie
import com.antoniocabezas.moviegoers.presentation.ui.adapters.recyclerview.MovieAdapter
import com.antoniocabezas.moviegoers.presentation.viewmodels.MainViewModel
import com.antoniocabezas.moviegoers.utils.navigation.Router
import com.antoniocabezas.moviegoers.utils.recyclerview.RecyclerViewItemClickListener
import kotlinx.android.synthetic.main.fragment_updated.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UpdatedFragment : BaseFragment(), SeekBar.OnSeekBarChangeListener {

    companion object {
        val TAG: String = UpdatedFragment::class.java.simpleName
    }

    private var mViewModel: MainViewModel? = null
    private lateinit var mUpdatedMoviesObserver: Observer<ArrayList<Movie>>
    private lateinit var mUpdatedMoviesInteractorStateObserver: Observer<Interactor.State>

    private lateinit var mMoviesAdapter: RecyclerView.Adapter<*>
    private lateinit var mMoviesRecyclerView: RecyclerView
    private lateinit var mMoviesLayoutManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "Executing life cycle event on create.")
        super.onCreate(savedInstanceState)
        this.mViewModel = activity?.let { ViewModelProviders.of(it).get(MainViewModel::class.java) }
        this.mViewModel?.mUpdatedMovies?.observe(this, this.getUpdatedMoviesObserver())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "Executing life cycle event on create view.")
        return inflater.inflate(R.layout.fragment_updated, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "Executing life cycle event on view created.")
        super.onViewCreated(view, savedInstanceState)
        this.setupViews()
        this.setupListeners()
        this.getUpdatedMovies(days_seek_bar.max, 1)
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        this.setLastDaysLabel(progress)
        if (progress == 0) this.setSeekBarValue(1) else this.getUpdatedMovies(days_seek_bar.max, progress)
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }

    private fun setupViews() {
        Log.d(TAG, "Setting up views.")
        this.setupSeekBar()
        this.mMoviesLayoutManager = GridLayoutManager(context, 3)
        this.mMoviesAdapter = MovieAdapter(this.mViewModel?.mUpdatedMovies, context)
        this.mMoviesRecyclerView = result_list.apply {
            layoutManager = mMoviesLayoutManager
            adapter = mMoviesAdapter
            itemAnimator = DefaultItemAnimator()
        }

    }

    private fun setupSeekBar() {
        Log.d(TAG, "Setting up seek bar.")
        val mMaxValue = days_seek_bar.max
        max_days_label.text = mMaxValue.toString()

        this.setMinSeekBarValue()
        this.setSeekBarValue(mMaxValue)
        this.setLastDaysLabel(mMaxValue)
    }

    private fun setLastDaysLabel(days: Int) {
        when (days) {
            1 -> in_the_last_days.text = resources.getString(R.string.in_last_day)
            else -> in_the_last_days.text = resources.getString(R.string.in_last_x_days, days)
        }
    }

    private fun setMinSeekBarValue() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            min_days_label.text = days_seek_bar.min.toString()
        } else {
            min_days_label.text = "0"
        }
    }

    private fun setSeekBarValue(value: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            days_seek_bar.setProgress(value, true)
        } else {
            days_seek_bar.progress = value
        }
    }

    private fun setupListeners() {
        Log.d(TAG, "Setting up listeners.")
        days_seek_bar.setOnSeekBarChangeListener(this)
        context?.let {
            this.mMoviesRecyclerView.addOnItemTouchListener(
                RecyclerViewItemClickListener(
                    it,
                    this.mMoviesRecyclerView,
                    object : RecyclerViewItemClickListener.RecyclerTouchListener {
                        override fun onClickItem(v: View, position: Int) {
                            mViewModel?.mUpdatedMovies?.value?.get(position)?.id?.let { mMovieId ->
                                handleMovieAction(mMovieId.toString())
                            }
                        }

                        override fun onLongClickItem(v: View, position: Int) {

                        }

                    })
            )
        }

    }

    private fun getUpdatedMoviesObserver(): Observer<ArrayList<Movie>> {
        return if (::mUpdatedMoviesObserver.isInitialized) {
            this.mUpdatedMoviesObserver
        } else {
            this.mUpdatedMoviesObserver = Observer {
                Log.d(TAG, it.toString())
                mMoviesAdapter.notifyDataSetChanged()
                Handler().postDelayed({
                    hideShimmer()
                }, 1000)
            }
            this.mUpdatedMoviesObserver
        }
    }

    private fun getUpdatedMoviesInteractorStateObserver(): Observer<Interactor.State> {
        return if (::mUpdatedMoviesInteractorStateObserver.isInitialized) {
            this.mUpdatedMoviesInteractorStateObserver
        } else {
            this.mUpdatedMoviesInteractorStateObserver = Observer {
                when (it) {
                    Interactor.State.LOADING -> {
                        this.onLoading()
                        showShimmer()
                    }
                    Interactor.State.UNEXPECTED -> {
                    }
                    else -> {
                    }
                }
            }
            this.mUpdatedMoviesInteractorStateObserver
        }
    }

    private fun hideShimmer() {
        this.onError()
        shimmer.stopShimmer()
        shimmer.visibility = View.GONE
        result_list.visibility = View.VISIBLE
    }

    private fun showShimmer() {
        result_list.visibility = View.GONE
        shimmer.startShimmer()
        shimmer.visibility = View.VISIBLE
    }

    private fun onLoading() {

    }

    private fun onResultsReceived() {

    }

    private fun onError() {

    }

    private fun handleMovieAction(movieId: String) {
        Router.navigateToMovieDetails(activity as AppCompatActivity, movieId)
    }

    private fun getUpdatedMovies(daysAgo: Int, page: Int) {
        GlobalScope.launch {
            mViewModel?.getUpdatedMovies(daysAgo, page)
        }
        showShimmer()
    }
}
