package com.antoniocabezas.moviegoers.di.components

import com.antoniocabezas.moviegoers.di.modules.ViewModelModule
import com.antoniocabezas.moviegoers.presentation.viewmodels.DetailsViewModel
import com.antoniocabezas.moviegoers.presentation.viewmodels.MainViewModel
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(ViewModelModule::class))
interface ViewModelComponent {
    fun inject(mainViewModel: MainViewModel)
    fun inject(detailsViewModel: DetailsViewModel)
}