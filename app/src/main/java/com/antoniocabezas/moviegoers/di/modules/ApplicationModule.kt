package com.antoniocabezas.moviegoers.di.modules

import android.content.Context
import android.content.res.Resources
import com.antoniocabezas.moviegoers.MoviegoersApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {
    @Provides
    @Singleton
    fun provideAppContext(): Context = MoviegoersApp.instance

    @Provides
    @Singleton
    fun provideResources(context: Context): Resources = context.resources
}