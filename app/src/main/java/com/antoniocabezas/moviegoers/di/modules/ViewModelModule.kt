package com.antoniocabezas.moviegoers.di.modules

import com.antoniocabezas.moviegoers.presentation.viewmodels.base.BaseViewModel
import dagger.Module
import dagger.Provides

@Module
class ViewModelModule(viewModel: BaseViewModel) {

    val mViewModel: BaseViewModel = viewModel

    @Provides
    fun provideViewModel(): BaseViewModel = this.mViewModel
}