package com.antoniocabezas.moviegoers.di.components

import com.antoniocabezas.moviegoers.di.modules.InteractorModule
import com.antoniocabezas.moviegoers.domain.interactors.implementations.GetMovieDetailsInteractorImpl
import com.antoniocabezas.moviegoers.domain.interactors.implementations.GetUpdatedMoviesInteractorImpl
import dagger.Subcomponent


@Subcomponent(modules = arrayOf(InteractorModule::class))
interface InteractorComponent {
    fun inject(getUpdatedMoviesInteractor: GetUpdatedMoviesInteractorImpl)
    fun inject(getMovieDetailsInteractor: GetMovieDetailsInteractorImpl)
}