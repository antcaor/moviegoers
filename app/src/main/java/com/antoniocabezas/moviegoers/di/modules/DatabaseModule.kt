package com.antoniocabezas.moviegoers.di.modules

import android.arch.persistence.room.Room
import com.antoniocabezas.moviegoers.MoviegoersApp
import com.antoniocabezas.moviegoers.data.MovieDataSource
import com.antoniocabezas.moviegoers.data.database.MovieDAO
import com.antoniocabezas.moviegoers.data.database.MovieDatabase
import com.antoniocabezas.moviegoers.data.repository.MovieRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideMoviegoersDatabase(app: MoviegoersApp): MovieDatabase =
        Room.databaseBuilder(app, MovieDatabase::class.java, "MovieDatabase").build()

    @Provides
    @Singleton
    fun provideMoviesDao(database: MovieDatabase): MovieDAO = database.movieDao()

    @Provides
    @Singleton
    fun providesMovieRepository(): MovieRepository {
        return MovieDataSource()
    }
}