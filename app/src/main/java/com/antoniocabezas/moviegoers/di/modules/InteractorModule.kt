package com.antoniocabezas.moviegoers.di.modules

import com.antoniocabezas.moviegoers.domain.interactors.base.BaseInteractor
import com.antoniocabezas.moviegoers.domain.interactors.base.Interactor
import dagger.Module
import dagger.Provides

@Module
class InteractorModule(interactor: BaseInteractor) {

    val mInteractor: BaseInteractor = interactor

    @Provides
    fun provideInteractor(): Interactor = this.mInteractor
}