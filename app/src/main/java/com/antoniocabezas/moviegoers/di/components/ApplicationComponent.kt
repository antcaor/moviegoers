package com.antoniocabezas.moviegoers.di.components

import com.antoniocabezas.moviegoers.di.modules.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        ApplicationModule::class,
        RetrofitModule::class,
        OkHttpClientModule::class,
        DatabaseModule::class
    )
)
interface ApplicationComponent {
    fun plus(viewModelModule: ViewModelModule): ViewModelComponent
    fun plus(interactorModule: InteractorModule): InteractorComponent
}