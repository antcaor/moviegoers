package com.antoniocabezas.moviegoers.di.modules

import android.util.Log
import com.antoniocabezas.moviegoers.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
class OkHttpClientModule {
    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val mOkHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()

        val mAuthInterceptor = Interceptor { chain ->
            val mAuthorizedUrl = chain.request().url()
                .newBuilder()
                .addQueryParameter("api_key", BuildConfig.tmdbApiKey)
                .build()

            val mNewRequest = chain.request()
                .newBuilder()
                .url(mAuthorizedUrl)
                .build()

            chain.proceed(mNewRequest)
        }

        val mLogger = HttpLoggingInterceptor()

        if (BuildConfig.DEBUG) {
            mLogger.level = HttpLoggingInterceptor.Level.BODY
            mOkHttpClientBuilder.addInterceptor(mLogger)
        }

        mOkHttpClientBuilder.addInterceptor(mAuthInterceptor)
        return mOkHttpClientBuilder.build()
    }
}