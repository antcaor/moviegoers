package com.antoniocabezas.moviegoers

import android.app.Application
import android.util.Log
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent
import com.antoniocabezas.moviegoers.di.components.DaggerApplicationComponent
import com.facebook.stetho.Stetho

class MoviegoersApp : Application() {

    companion object {
        val TAG: String = MoviegoersApp::class.java.simpleName
        @JvmStatic
        lateinit var appComponent: ApplicationComponent
        @JvmStatic
        lateinit var instance: MoviegoersApp
    }

    override fun onCreate() {
        Log.d(TAG, "Executing life cycle event on create.")
        super.onCreate()
        instance = this
        this.setupAppComponent()
        Stetho.initializeWithDefaults(this);
    }

    private fun setupAppComponent() {
        Log.d(TAG, "Setting up app component.")
        appComponent = DaggerApplicationComponent.create()
    }

}