package com.antoniocabezas.moviegoers.data.database

import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE

interface BaseDAO<T> {
    @Insert(onConflict = REPLACE)
    fun insert(t: T)

    @Insert(onConflict = REPLACE)
    fun insert(t: List<T>)
}