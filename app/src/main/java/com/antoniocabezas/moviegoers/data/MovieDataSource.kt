package com.antoniocabezas.moviegoers.data

import android.arch.lifecycle.LiveData
import com.antoniocabezas.moviegoers.data.database.MovieDAO
import com.antoniocabezas.moviegoers.data.repository.MovieRepository
import com.antoniocabezas.moviegoers.domain.models.Movie
import javax.inject.Inject

class MovieDataSource : MovieRepository {

    companion object {
        val TAG: String = MovieDataSource::class.java.simpleName
    }

    @Inject
    lateinit var mMovieDAO: MovieDAO

    override fun findById(id: Int): LiveData<List<Movie>> {
        return mMovieDAO.findById(id)
    }

    override fun findAll(): LiveData<List<Movie>> {
        return mMovieDAO.findAll()
    }

    override fun insert(movie: Movie) {
        return mMovieDAO.insert(movie)
    }
}
