package com.antoniocabezas.moviegoers.data.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.antoniocabezas.moviegoers.domain.models.Movie

@Dao
interface MovieDAO : BaseDAO<Movie> {

    @Query("SELECT * FROM Movies WHERE id=:id")
    fun findById(id: Int): LiveData<List<Movie>>

    @Query("SELECT * FROM Movies")
    fun findAll(): LiveData<List<Movie>>

    @Query("DELETE FROM Movies")
    fun deleteAll()
}