package com.antoniocabezas.moviegoers.data.repository

import android.arch.lifecycle.LiveData
import com.antoniocabezas.moviegoers.domain.models.Movie

interface MovieRepository {

    fun findById(id: Int): LiveData<List<Movie>>
    fun findAll(): LiveData<List<Movie>>
    fun insert(movie: Movie)

}