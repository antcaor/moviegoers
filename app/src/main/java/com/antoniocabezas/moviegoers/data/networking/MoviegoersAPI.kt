package com.antoniocabezas.moviegoers.data.networking

import com.antoniocabezas.moviegoers.domain.models.Movie
import com.antoniocabezas.moviegoers.domain.models.UpdatedMovieResult
import com.antoniocabezas.moviegoers.domain.models.UpdatedMoviesResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviegoersAPI {
    @GET("/3/movie/changes")
    fun getUpdatedMovies(
        @Query("end_date") endDate: String,
        @Query("start_date") startDate: String,
        @Query("page") page: Int
    ): Single<UpdatedMoviesResponse>

    @GET("/3/movie/{movie_id}")
    fun getMovieDetails(
        @Path("movie_id") movieId: Int
    ): Single<Movie>
}