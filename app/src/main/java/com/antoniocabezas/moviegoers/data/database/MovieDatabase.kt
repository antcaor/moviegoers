package com.antoniocabezas.moviegoers.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.antoniocabezas.moviegoers.domain.models.Genre
import com.antoniocabezas.moviegoers.domain.models.Movie

@Database(entities = [Movie::class, Genre::class], version = 1, exportSchema = false)
@TypeConverters(GenreConverter::class)
abstract class MovieDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDAO
}