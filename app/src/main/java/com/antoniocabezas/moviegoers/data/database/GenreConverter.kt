package com.antoniocabezas.moviegoers.data.database

import android.arch.persistence.room.TypeConverter
import com.antoniocabezas.moviegoers.domain.models.Genre
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

class GenreConverter {
  @TypeConverter
  fun fromList(list: List<Genre>): String? {
    val mListData = Types.newParameterizedType(List::class.java, Genre::class.java)
    return Moshi.Builder().build().adapter<List<Genre>>(mListData).toJson(list)
  }
  
  @TypeConverter
  fun toList(listString: String): List<Genre>? {
    val mListData = Types.newParameterizedType(List::class.java, Genre::class.java)
    return Moshi.Builder().build().adapter<List<Genre>>(mListData).fromJson(listString)
  }
}