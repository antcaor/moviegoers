package com.antoniocabezas.moviegoers.domain.interactors.base

interface Interactor {
    enum class State {
        LOADING,
        SUCCESSFUL,
        UNEXPECTED
    }

    enum class Error {
        NULL_RESOURCE,
        INVALID_KEY,
        INVALID_PARAMS,
        INTERACTOR_EXCEPTION,
        INTERNAL_SERVER_ERROR,
        SUBSCRIPTION_ERROR
    }

    suspend fun execute()

    fun postLoading()
    fun postFailure(error: Interactor.Error)
    fun <T> postSuccess(response: T)
}