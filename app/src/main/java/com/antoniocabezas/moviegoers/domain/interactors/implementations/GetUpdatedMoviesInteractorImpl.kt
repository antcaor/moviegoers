package com.antoniocabezas.moviegoers.domain.interactors.implementations

import android.util.Log
import com.antoniocabezas.moviegoers.MoviegoersApp
import com.antoniocabezas.moviegoers.data.networking.MoviegoersAPI
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent
import com.antoniocabezas.moviegoers.di.modules.InteractorModule
import com.antoniocabezas.moviegoers.domain.interactors.GetUpdatedMoviesInteractor
import com.antoniocabezas.moviegoers.domain.interactors.base.BaseInteractor
import com.antoniocabezas.moviegoers.domain.interactors.base.Interactor
import com.antoniocabezas.moviegoers.domain.models.Movie
import com.antoniocabezas.moviegoers.domain.models.UpdatedMovieResult
import com.antoniocabezas.moviegoers.domain.models.UpdatedMoviesResponse
import com.antoniocabezas.moviegoers.utils.DateUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class GetUpdatedMoviesInteractorImpl(
    val callback: GetUpdatedMoviesInteractor.GetUpdatedMoviesCallback,
    override val coroutineContext: CoroutineContext
) : BaseInteractor(),
    GetUpdatedMoviesInteractor {

    companion object {
        val TAG: String = GetUpdatedMoviesInteractorImpl::class.java.simpleName
        const val REQUEST_LIMIT: Long = 39
    }

    @Inject
    lateinit var mApi: MoviegoersAPI

    private var mDaysAgo: Int = 0
    private var mPage: Int = 1

    override fun injectDependencies(applicationComponent: ApplicationComponent) {
        applicationComponent.plus(InteractorModule(this)).inject(this)
    }

    init {
        injectDependencies(MoviegoersApp.appComponent)
    }

    override fun setParams(daysAgo: Int, page: Int) {
        this.mDaysAgo = daysAgo
        this.mPage = page
    }

    override fun postLoading() {
        Log.d(TAG, "Interactor post loading.")
        callback.onGettingUpdatedMovies()
    }

    override fun postFailure(error: Interactor.Error) {
        Log.d(TAG, "Interactor post error: $error.")
        callback.onGettingUpdatedMoviesFailure(error)
    }

    override fun <T> postSuccess(response: T) {
        Log.d(TAG, "Interactor post success.")
        if (response is Movie) {
            callback.onUpdatedMovieReceived(response)
        }
    }

    override suspend fun run() {

        Log.d(TAG, "Running.")

        val mRequest = mApi.getUpdatedMovies(
            DateUtils.getCalculatedDate("yyyy-MM-dd", 0),
            DateUtils.getCalculatedDate("yyyy-MM-dd", -mDaysAgo),
            mPage
        )


        val mDisposableResponse = mRequest.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMapObservable { response: UpdatedMoviesResponse -> Observable.fromIterable(response.results) }
            .filter { result: UpdatedMovieResult -> result.adult == false }
            .doOnError { Log.d(TAG, "Error: ${it.message} ${it.cause}.") }
            .take(REQUEST_LIMIT)
            .subscribe { result: UpdatedMovieResult ->
                result.id?.let {
                    mApi.getMovieDetails(result.id.toInt()).subscribeOn(Schedulers.io())
                        .delay(30, TimeUnit.MICROSECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError { Log.d(TAG, "Error: ${it.message} ${it.cause}.") }
                        .subscribe { movie: Movie? ->
                            movie?.let {
                                postSuccess(it)
                            }
                        }
                }
            }


    }
    //TODO get all movie ids and refresh its information in room database.

}
