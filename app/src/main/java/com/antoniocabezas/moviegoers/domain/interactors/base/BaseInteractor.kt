package com.antoniocabezas.moviegoers.domain.interactors.base

import android.util.Log
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent
import kotlinx.coroutines.*

abstract class BaseInteractor : Interactor, CoroutineScope {

    companion object {
        val TAG: String = BaseInteractor::class.java.simpleName
    }

    @Volatile
    protected var mIsCanceled: Boolean = false
    @Volatile
    protected var mIsRunning: Boolean = false

    abstract fun injectDependencies(applicationComponent: ApplicationComponent)

    fun cancel() {
        mIsCanceled = true
        mIsRunning = false
    }

    fun isRunning(): Boolean {
        return mIsRunning
    }

    fun onFinished() {
        mIsRunning = false
        mIsCanceled = false
    }

    override suspend fun execute() {
        coroutineScope {
            launch {
                try {
                    mIsRunning = true
                    withContext(Dispatchers.IO) {
                        run()
                    }
                } catch (exception: Exception) {
                    Log.e(TAG, "Exception: ${exception.message} cause: ${exception.cause}")
                }
            }
        }

    }

    abstract suspend fun run()

}