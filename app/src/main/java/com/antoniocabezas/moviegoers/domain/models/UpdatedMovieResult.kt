package com.antoniocabezas.moviegoers.domain.models

import com.squareup.moshi.Json

data class UpdatedMovieResult(
    @Json(name = "id")
    val id: String?,
    @Json(name = "adult")
    val adult: Boolean?
)