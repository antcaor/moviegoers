package com.antoniocabezas.moviegoers.domain.interactors.implementations

import android.util.Log
import com.antoniocabezas.moviegoers.data.networking.MoviegoersAPI
import com.antoniocabezas.moviegoers.di.components.ApplicationComponent
import com.antoniocabezas.moviegoers.di.modules.InteractorModule
import com.antoniocabezas.moviegoers.domain.interactors.GetMovieDetailsInteractor
import com.antoniocabezas.moviegoers.domain.interactors.base.BaseInteractor
import com.antoniocabezas.moviegoers.domain.interactors.base.Interactor
import com.antoniocabezas.moviegoers.domain.models.Movie
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class GetMovieDetailsInteractorImpl(
    val callback: GetMovieDetailsInteractor.GetMovieDetailsCallback,
    override val coroutineContext: CoroutineContext
) : BaseInteractor(),
    GetMovieDetailsInteractor {

    companion object {
        val TAG: String = GetMovieDetailsInteractorImpl::class.java.simpleName
    }

    @Inject
    lateinit var mApi: MoviegoersAPI

    private var mMovieId: Int = -1

    override fun injectDependencies(applicationComponent: ApplicationComponent) {
        applicationComponent.plus(InteractorModule(this)).inject(this)
    }

    override fun setParams(movieId: Int) {
        this.mMovieId = movieId
    }

    override fun postLoading() {
        Log.d(TAG, "Interactor post loading.")
        callback.onGettingMovieDetails()
    }

    override fun postFailure(error: Interactor.Error) {
        Log.d(TAG, "Interactor post error: $error.")
        callback.onGettingMovieDetailsFailure(error)
    }

    override fun <T> postSuccess(response: T) {
        Log.d(TAG, "Interactor post success.")
        if (response is Movie) {
            callback.onGettingMovieDetailsSuccess(response)
        }
    }

    override suspend fun run() {
        Log.d(TAG, "Running.")

        val mRequest = mApi.getMovieDetails(
           mMovieId
        )

        /*val mResponse = mRequest.await()

        if (mResponse.isSuccessful) {
            Log.d(TAG, "Interactor success.")

            postSuccess(mResponse.body())
        } else {
            Log.d(TAG, "Interactor failed.")
        }*/
    }

}