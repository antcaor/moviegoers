package com.antoniocabezas.moviegoers.domain.interactors

import com.antoniocabezas.moviegoers.domain.interactors.base.Interactor
import com.antoniocabezas.moviegoers.domain.models.Movie

interface GetMovieListDetailInteractor : Interactor {
    fun setParams(movieIds: List<Int>)

    interface GetMovieDetailsCallback {
        fun onGettingMovieListDetails()
        fun onGettingMovieListDetailsFailure(error: Interactor.Error)
        fun onGettingMovieListDetailsSuccess(movieListDetails: List<Movie>)
    }
}