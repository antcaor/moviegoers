package com.antoniocabezas.moviegoers.domain.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import com.antoniocabezas.moviegoers.data.database.GenreConverter
import com.squareup.moshi.Json

@Entity(tableName = "Movies", indices = [Index("id"), Index("title")])
data class Movie(
    @Json(name = "id")
    @PrimaryKey
    val id: Int,
    @Json(name = "genres")
    @TypeConverters(GenreConverter::class)
    val genres: List<Genre>,
    @Json(name = "homepage")
    val homePage: String?,
    @Json(name = "adult")
    val adult: Boolean?,
    @Json(name = "poster_path")
    val posterPath: String?,
    @Json(name = "backdrop_path")
    val backdropPath: String?,
    @Json(name = "release_date")
    val releaseDate: String?,
    @Json(name = "status")
    val status: String?,
    @Json(name = "title")
    val title: String?,
    @Json(name = "video")
    val video: Boolean?,
    @Json(name = "vote_average")
    val voteAverage: Float?
)