package com.antoniocabezas.moviegoers.domain.interactors

import com.antoniocabezas.moviegoers.domain.interactors.base.Interactor

interface GetFavoriteFilmsInteractor: Interactor