package com.antoniocabezas.moviegoers.domain.interactors.base

interface InteractorCallback {
    fun onLoading()
    fun <T> onSuccess(response: T)
    fun onError(error: Interactor.Error)
}