package com.antoniocabezas.moviegoers.domain.interactors

import com.antoniocabezas.moviegoers.domain.interactors.base.Interactor
import com.antoniocabezas.moviegoers.domain.models.Movie

interface GetUpdatedMoviesInteractor : Interactor {
    fun setParams(daysAgo: Int, page: Int)

    interface GetUpdatedMoviesCallback {
        fun onGettingUpdatedMovies()
        fun onGettingUpdatedMoviesFailure(error: Interactor.Error)
        fun onUpdatedMovieReceived(movie: Movie)
    }
}