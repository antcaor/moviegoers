package com.antoniocabezas.moviegoers.domain.models

import com.squareup.moshi.Json

data class UpdatedMoviesResponse(
    @Json(name = "results")
    val results: List<UpdatedMovieResult>,
    @Json(name = "page")
    val page: Int,
    @Json(name = "total_pages")
    val totalPages: Int,
    @Json(name = "total_results")
    val totalResults: Int
)