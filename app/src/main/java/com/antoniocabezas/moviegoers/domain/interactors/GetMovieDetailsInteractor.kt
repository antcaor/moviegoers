package com.antoniocabezas.moviegoers.domain.interactors

import com.antoniocabezas.moviegoers.domain.interactors.base.Interactor
import com.antoniocabezas.moviegoers.domain.models.Movie

interface GetMovieDetailsInteractor : Interactor {
    fun setParams(movieId: Int)

    interface GetMovieDetailsCallback {
        fun onGettingMovieDetails()
        fun onGettingMovieDetailsFailure(error: Interactor.Error)
        fun onGettingMovieDetailsSuccess(movieDetails: Movie)
    }
}